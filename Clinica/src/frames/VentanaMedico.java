package frames;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTabbedPane;

import complementos.MiFrame;
import login.Login;
import paneles.Modificar;
import paneles.PanelHistorial;
import paneles.TablaCitas;
public class VentanaMedico extends MiFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTabbedPane pesta�as;
	private PanelHistorial ph;
	private Modificar mod;
	private TablaCitas tcitas;
	public VentanaMedico(String codigoMedico){
		super("M�dico");
		Salir.addActionListener(this);
		CerrarSesion.addActionListener(this);
		getContentPane().setLayout(new CardLayout(0, 0));
		pesta�as=new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(pesta�as);
		ph=new PanelHistorial(codigoMedico);
		mod=new Modificar(codigoMedico);
		tcitas=new TablaCitas(codigoMedico);
		pesta�as.addTab("Citas de hoy",null,tcitas,null);
		pesta�as.addTab("Modificar", null,mod, null);
		Thread t=new Thread(ph);
		pesta�as.addTab("Actual", null,ph, null);
		t.start();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(Salir)){
			System.exit(0);
		}
		if(e.getSource().equals(CerrarSesion)){
			this.dispose();
			new Login();
		}
	}
}
