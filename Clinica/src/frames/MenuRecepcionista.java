package frames;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import complementos.Conexion;
import complementos.Logo;
import complementos.MiFrame;
import login.Login;
import paneles.Consultorio;
import paneles.TablaAtencion;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
public class MenuRecepcionista extends MiFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btnBuscarCita;
	JButton btnRegistrar ;
	JPanel panel;
	Logo logo;
	JPanel panel1;
	private String codigo;
	private String area;
	private TablaAtencion tabla;
	Thread t;
	private JTextField textField;
	JButton btnRecepcionar;
	private String[] nombres;
	private String[] consultorios;
	private String[] codigos;
	private Consultorio c;
	private Thread tc;
	private Consultorio c1;
	private Thread tc1;
	private Consultorio c2;
	private Thread tc2;
	private Consultorio c3;
	private Thread tc3;
	public MenuRecepcionista(String codigo){
		super("Bienvenido");
		this.codigo=codigo;
		area=obtenerArea();
		nombres=new String[4];
		consultorios=new String [4];
		codigos=new String [4];
		obtenerMedicos();
		tabla=new TablaAtencion(area);
		tabla.setSize(440, 325);
		tabla.setLocation(12, 161);
		panel=new JPanel();
		panel1=new JPanel();
		Salir.addActionListener(this);
		CerrarSesion.addActionListener(this);
		this.setSize(929,604);
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(new GridLayout(1,2));
		panel.setLayout(null);
		getContentPane().add(panel);
		logo=new Logo();
		logo.setSize(370, 137);
		logo.setLocation(43, 12);
		panel.add(logo);
		
		btnRegistrar = new JButton("Registrar Paciente");
		btnRegistrar.addActionListener(this);
		btnRegistrar.setBounds(138, 498, 179, 23);
		panel.add(btnRegistrar);
		
		btnBuscarCita = new JButton("Buscar Cita");
		btnBuscarCita.setBounds(138, 533, 179, 23);
		btnBuscarCita.addActionListener(this);
		panel.add(btnBuscarCita);
		t=new Thread(tabla);
		panel.add(tabla);
		t.start();
		panel1.setLayout(null);
		getContentPane().add(panel1);
		
		textField = new JTextField();
		textField.setBounds(125, 12, 145, 22);
		panel1.add(textField);
		textField.setColumns(10);
		
		JLabel lblCodigoCita = new JLabel("Codigo Cita:");
		lblCodigoCita.setBounds(12, 15, 128, 15);
		panel1.add(lblCodigoCita);
		
		btnRecepcionar = new JButton("Recepcionar");
		btnRecepcionar.setBounds(282, 10, 137, 25);
		btnRecepcionar.addActionListener(this);
		panel1.add(btnRecepcionar);
		
		c=new Consultorio(nombres[0],consultorios[0],codigos[0]);
		c.setSize(407, 108);
		c.setLocation(12,46);
		tc=new Thread(c);
		panel1.add(c);
		tc.start();
		c1=new Consultorio(nombres[1],consultorios[1],codigos[1]);
		c1.setSize(407, 108);
		c1.setLocation(12,166);
		tc1=new Thread(c1);
		panel1.add(c1);
		tc1.start();
		c2=new Consultorio(nombres[2],consultorios[2],codigos[2]);
		c2.setSize(407, 108);
		c2.setLocation(12,286);
		tc2=new Thread(c2);
		panel1.add(c2);
		tc2.start();
		c3=new Consultorio(nombres[3],consultorios[3],codigos[3]);
		c3.setSize(407, 108);
		c3.setLocation(12,406);
		tc3=new Thread(c3);
		panel1.add(c3);
		tc3.start();
		this.setVisible(true);
		this.setLocationRelativeTo(null);
	}
	private void obtenerMedicos() {
		// TODO Auto-generated method stub
		int contador=0;
		try{
			Connection cn=Conexion.getConexionMYSQL();
			Statement st=cn.createStatement();
			ResultSet rs=st.executeQuery("SELECT * FROM medicos inner join persona on medicos.codigoMedico=persona.codigo "
					+ "where medicos.especialidad='"+this.area+"';");
			while(rs.next()){
				nombres[contador]=new String(rs.getString("nombres")+" "+rs.getString("apellidos"));
				consultorios[contador]=new String(String.valueOf(rs.getInt("numeroConsultorio")));
				codigos[contador]=new String(rs.getString("codigoMedico"));
				contador++;
			}
			cn.close();
			st.close();
			rs.close();
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null,"Fallo en obtener los medicos");
		}
	}
	private String obtenerArea() {
		// TODO Auto-generated method stub
		String area=null;
		try{
			Connection cn=Conexion.getConexionMYSQL();
			Statement st=cn.createStatement();
			ResultSet rs=st.executeQuery("select * from areas inner join usuarios on areas.codigo=usuarios.codigo where areas.codigo='"+this.codigo+"';");
			if(rs.next()){
				area=rs.getString("areaAtencion");
			}
			cn.close();
			st.close();
			rs.close();
			return area;
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null,"Fallo en obtener la especialidad");
			return area;
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(btnRecepcionar)){
			asignarPaciente(textField.getText());
		}
		if(e.getSource().equals(btnRegistrar)){
			new RegistrarPaciente();
		}
		else if(e.getSource().equals(btnBuscarCita)){
			new BuscarCita();
		}
		else if(e.getSource().equals(Salir)){
			System.exit(0);
		}
		else if(e.getSource().equals(CerrarSesion)){
			this.dispose();
			new Login();
		}
	}
	private void asignarPaciente(String codigoCita) {
		// TODO Auto-generated method stub
		String codigoMedico = null;
		String codigoPaciente = null;
		try{
			Connection cn=Conexion.getConexionMYSQL();
			Statement st=cn.createStatement();
			ResultSet rs1=st.executeQuery("select codigoPaciente,codigoMedico from citas where codigoCita='"+codigoCita+"';");
			if(rs1.next()){
			codigoMedico=rs1.getString("codigoMedico");
			codigoPaciente=rs1.getString("codigoPaciente");
			}
			ResultSet rs=st.executeQuery("select * from temporal where codigoMedico='"+codigoMedico+"';");
			if(rs.next()){
				JOptionPane.showMessageDialog(null,"Hay un paciente en atencion");
				textField.setText("");
			}
			else{
				Statement st1=cn.createStatement();
				st1.executeUpdate("insert into temporal (codigoMedico, codigoPaciente) values ('"
						+codigoMedico+"','"
						+codigoPaciente+"');");
				st1.close();
				JOptionPane.showConfirmDialog(null,"Recepcionado con exito");
				textField.setText("");
			}
			rs1.close();
			cn.close();
			st.close();
			rs.close();
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null,"Fallo en asignar paciente");
		}
	}
}
