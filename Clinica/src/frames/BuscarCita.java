/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frames;

import informacion.Cita;
import informacion.Persona;
import javax.swing.JOptionPane;

public class BuscarCita extends javax.swing.JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = -210233654669095557L;
	Cita c = null;
    Persona p = null;
    Cita cita;

    public BuscarCita() {
    	super("Buscar Cita");
        initComponents();
        this.setLocationRelativeTo(null);
    }
    private void initComponents() {
        jInternalFrame2 = new javax.swing.JInternalFrame();
        btnBuscarCita = new javax.swing.JButton();
        txtmostrar = new javax.swing.JTextField();
        jlbingresar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jInternalFrame2.setVisible(true);

        btnBuscarCita.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        btnBuscarCita.setText("BUSCAR");
        btnBuscarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarCitaActionPerformed(evt);
            }
        });

        jlbingresar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jlbingresar.setText("INGRESE DNI : ");

        javax.swing.GroupLayout jInternalFrame2Layout = new javax.swing.GroupLayout(jInternalFrame2.getContentPane());
        jInternalFrame2.getContentPane().setLayout(jInternalFrame2Layout);
        jInternalFrame2Layout.setHorizontalGroup(
            jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame2Layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(jlbingresar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtmostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addComponent(btnBuscarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        jInternalFrame2Layout.setVerticalGroup(
            jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrame2Layout.createSequentialGroup()
                .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jInternalFrame2Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(jInternalFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jlbingresar)
                            .addComponent(txtmostrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jInternalFrame2Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(btnBuscarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(111, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jInternalFrame2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jInternalFrame2)
        );

        pack();
        this.setVisible(true);
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarCitaActionPerformed
        c = new Cita();
        cita = c.Buscar(txtmostrar.getText());
        p = c.BuscarPaciente(txtmostrar.getText());
        if (cita == null) {
            JOptionPane.showMessageDialog(null, "Paciente no encontrado", "", JOptionPane.INFORMATION_MESSAGE);
            int opcion = JOptionPane.showConfirmDialog(null, "Desea sacar cita ", "", JOptionPane.INFORMATION_MESSAGE);
            if (opcion == 0) {
                RegistrarPaciente con = new RegistrarPaciente();
                con.setVisible(true);
                this.dispose();
            } else if (opcion == 1 || opcion == 2) {
                this.dispose();
            }
        } else {
            DatosGeneralesPaciente con = new DatosGeneralesPaciente(p,cita);
            con.setVisible(true);
        }
    }//GEN-LAST:event_btnBuscarCitaActionPerformed
   

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnBuscarCita;
    public javax.swing.JInternalFrame jInternalFrame2;
    public javax.swing.JLabel jlbingresar;
    public javax.swing.JTextField txtmostrar;
    // End of variables declaration//GEN-END:variables
}
