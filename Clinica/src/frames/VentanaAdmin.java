package frames;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import complementos.MiFrame;
import login.Login;
import paneles.Buscar;
import paneles.Registro;
public class VentanaAdmin extends MiFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTabbedPane pesta�as=null;
	Registro r;
	Buscar b;
	public VentanaAdmin(){
		super("Bienvenido, Administrador");
		Salir.addActionListener(this);
		CerrarSesion.addActionListener(this);
		getContentPane().setLayout(new CardLayout(0, 0));
		pesta�as=new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(pesta�as);
		JPanel panel = new JPanel();
		pesta�as.addTab("Registro", null, panel, null);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0};
		gbl_panel.rowHeights = new int[]{0};
		gbl_panel.columnWeights = new double[]{Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		r=new Registro();
		b=new Buscar();
		pesta�as.addTab("Buscar", null, b, null);
		panel.add(r);
		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(Salir)){
			System.exit(0);
		}
		if(e.getSource().equals(CerrarSesion)){
			this.dispose();
			new Login();
		}
	}
}
