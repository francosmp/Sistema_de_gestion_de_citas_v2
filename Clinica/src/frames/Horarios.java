package frames;

import complementos.Conexion;
import complementos.registrarBasedatos;
import informacion.Horario;
import informacion.Medico;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Horarios extends javax.swing.JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	RegistrarPaciente rp;
    DefaultTableModel modelo;
    Horario hor;
    String resultado = "";
    String codigoH="";
    public Horarios() {
        initComponents();
    }
    private void initComponents() {

        panel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtDatosMedicos = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaHorario = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel1.setText("Medico ");

        txtDatosMedicos.setEnabled(false);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel2.setText("Disponibilidad");

        tablaHorario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaHorario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaHorarioMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaHorario);

        jButton1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButton1.setText("guadar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(22, 22, 22)
                            .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtDatosMedicos, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(36, 36, 36)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(63, Short.MAX_VALUE))
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtDatosMedicos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jButton1)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        this.setVisible(true);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
    public Time getHorario() {
        return Time.valueOf(resultado);
    }
    private void tablaHorarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaHorarioMouseClicked
        int fila = tablaHorario.getSelectedRow();
        System.out.println(fila);
        if (fila >= 0) {
            resultado = String.valueOf(tablaHorario.getValueAt(fila, 1));
            codigoH=String.valueOf(tablaHorario.getValueAt(fila,0));
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione al menos una fila");
        }
    }//GEN-LAST:event_tablaHorarioMouseClicked

    public void datosTabla(Medico medico, Date fec) {
        Object[] nombreColumnas = {"Hora", "Disponibilidad"};
        String[][] data = null;
        modelo = new DefaultTableModel(data, nombreColumnas);
        rp = new RegistrarPaciente();
        Object[] horas = getHorario(medico.getCodigoM(), fec);
        llenarTabla(modelo, horas);
        Object[] hor = getEstado(medico.getCodigoM(), fec);
        for (int i = 0; i < hor.length; i++) {
            if (hor[i].equals("T1") || hor[i].equals("T2") || hor[i].equals("T3") || hor[i].equals("T4") || hor[i].equals("T5") && hor[i].equals("T6")
                    || hor[i].equals("T7") || hor[i].equals("T8") || hor[i].equals("T9") ||hor[i].equals("T10") || hor[i].equals("T11")
                    || hor[i].equals("T12") || hor[i].equals("T13") || hor[i].equals("T14") || hor[i].equals("T15") || hor[i].equals("T16")) {
                modelo.setValueAt("", i, 1);
            } else {
                modelo.setValueAt("Disponible", i, 1);
            }
        }
        tablaHorario.setModel(modelo);
    }

    private Object[] getEstado(String cod, Date fe) {
        Object[] h = new Object[16];
        int i = 0;
        Connection conectar = Conexion.getConexionMYSQL();
        try {
            PreparedStatement ps = conectar.prepareStatement("select * from turnos where (codigoMedico=? and dia=?)");
            ps.setString(1, cod);
            ps.setDate(2, fe);
            ResultSet rst = ps.executeQuery();
            while (rst.next()) {
                h[i] = (rst.getString(2));
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return h;
    }

    private Object[] getHorario(String codMedico, Date fecha) {
        Object[] h = new Object[16];
        int i = 0;
        Connection conectar = Conexion.getConexionMYSQL();
        try {
            PreparedStatement ps = conectar.prepareStatement("select * from horarios inner join clinica.turnos where (turno.codigoMedico=" + codMedico + " AND turnos.dia=" + fecha + ")");
            ps.setString(1, codMedico);
            ps.setDate(2, fecha);
            ResultSet rst = ps.executeQuery();
            while (rst.next()) {
                h[i] = (rst.getInt(2));
                i++;
            }

        } catch (SQLException ex) {
            Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return h;
    }

    private void llenarTabla(DefaultTableModel modelo, Object[] horas) {
        int k = 0;
        for (Object o : horas) {
            modelo.insertRow(k, new Object[]{});
            modelo.setValueAt(o, k, 0);
            k++;
        }
        tablaHorario.setModel(modelo);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panel1;
    private javax.swing.JTable tablaHorario;
    public javax.swing.JTextField txtDatosMedicos;
    // End of variables declaration//GEN-END:variables

}
