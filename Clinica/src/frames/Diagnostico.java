package frames;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

import informacion.Historia;

public class Diagnostico extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel diag;
	JLabel diagp;
	JLabel doc;
	JLabel receta;
	JLabel recetap;
	JLabel especial;
	JLabel numc;
	public Diagnostico(Historia h){
		super("Detalles de la Cita:"+h.getCita().getFechaCita());
		this.getContentPane().setLayout(new GridLayout(5,0));
		doc=new JLabel("M�dico: "+h.getCita().getMedico().getNombre()+" "+h.getCita().getMedico().getApellido());
		this.add(doc);
		especial=new JLabel("Especialidad: "+h.getCita().getMedico().getEspecialidad());
		this.add(especial);
		numc=new JLabel("Numero de consultorio: "+h.getCita().getMedico().getNumConsultorio());
		this.add(numc);
		diag=new JLabel("Diagnostico: "+h.getDiagnostico());
		this.add(diag);
		receta=new JLabel("Receta: "+h.getReceta());
		this.add(receta);
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

}
