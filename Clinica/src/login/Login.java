package login;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import complementos.Conexion;
import complementos.Logo;
import complementos.LogoFrame;
import frames.MenuRecepcionista;
import frames.VentanaAdmin;
import frames.VentanaMedico;
public class Login extends LogoFrame implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static JTextField txtUsuario;
	static JPasswordField txtPassword; 
	JLabel lblUsuario, lblPassword;
	JButton btnAceptar;
	Boolean b;
	String codigo=null;
	String codigo1=null;
	JButton cerrar;
	GridBagLayout gbl = null;
	
	public Login(){
		super("Cl�nica Walde");
		gbl = new GridBagLayout();
		gbl.columnWidths = new int[]{120, 0, 0, 120};
		gbl.rowHeights = new int[]{110, 15, 0, 0, 34, 50};
		getContentPane().setLayout(gbl);
		getContentPane().setBackground(Color.white);
		Init();
	}
	private void Init() {
		Logo miLogo = new Logo();
		GridBagConstraints gbc_logo = new GridBagConstraints();
		gbc_logo.gridwidth = 4;
		gbc_logo.fill=GridBagConstraints.BOTH;
		gbc_logo.gridx=0;
		gbc_logo.gridy=0;
		gbc_logo.insets = new Insets(5, 5, 5, 0);
		getContentPane().add(miLogo, gbc_logo);

		txtUsuario = new JTextField();
		
		GridBagConstraints gbc_txtUsuario = new GridBagConstraints();
		gbc_txtUsuario.gridx=2;
		gbc_txtUsuario.gridy=2;
		gbc_txtUsuario.insets=new Insets(5, 5, 5, 5);
		txtUsuario.setColumns(15);
		txtUsuario.addActionListener(this);
		lblUsuario = new JLabel("USUARIO");
		
		
		GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
		gbc_lblUsuario.anchor = GridBagConstraints.WEST;
		gbc_lblUsuario.gridx=1;
		gbc_lblUsuario.gridy=2;
		gbc_lblUsuario.insets=new Insets(5,5,5,5);
		getContentPane().add(lblUsuario, gbc_lblUsuario);
		getContentPane().add(txtUsuario, gbc_txtUsuario);
		
		txtPassword=new JPasswordField();

				
		GridBagConstraints gbc_txtContrase�a = new GridBagConstraints();
		gbc_txtContrase�a.gridx=2;
		gbc_txtContrase�a.gridy=3;
		gbc_txtContrase�a.insets=new Insets(5, 5, 5, 5);
		txtPassword.setColumns(15);
		txtPassword.addActionListener(this);
				
		lblPassword= new JLabel("PASSWORD");
				
		GridBagConstraints gbc_lblContrase�a = new GridBagConstraints();
		gbc_lblContrase�a.anchor = GridBagConstraints.WEST;
		gbc_lblContrase�a.gridx=1;
		gbc_lblContrase�a.gridy=3;
		gbc_lblContrase�a.insets=new Insets(5,5,5,5);
		getContentPane().add(lblPassword, gbc_lblContrase�a);
		getContentPane().add(txtPassword, gbc_txtContrase�a);
		
		btnAceptar =new JButton("Aceptar");
		
		GridBagConstraints gbc_btnIngresar = new GridBagConstraints();
		gbc_btnIngresar.fill = GridBagConstraints.VERTICAL;
		gbc_btnIngresar.anchor = GridBagConstraints.EAST;
		gbc_btnIngresar.gridx=2;
		gbc_btnIngresar.gridy=4;
		gbc_btnIngresar.insets=new Insets(5, 5, 5, 5);
		btnAceptar.addActionListener(this);
		getContentPane().add(btnAceptar, gbc_btnIngresar);
		
        this.setVisible(true);
        this.setResizable(false);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
		
	
	public static void NuevoIngreso() {
		// TODO Auto-generated method stub
		txtPassword.setText("");
		txtUsuario.setText("");
		txtUsuario.requestFocus();
	}
	public void BuscarUsuario(String usu, String passw){
		int priv=0;
		String cod=null;
		try{
		Connection cn=Conexion.getConexionMYSQL();
		Statement st= cn.createStatement();
		ResultSet rs= st.executeQuery("SELECT usuario FROM usuarios WHERE usuario ='"+usu+"'");
		if(rs.next()){
				Statement st1=cn.createStatement();
				ResultSet rs1= st1.executeQuery("Select privilegios,codigo FROM usuarios WHERE usuario='"+usu+"'");
				while(rs1.next()){
					priv=rs1.getInt(1);
					cod=rs1.getString("codigo");
				}
				if(verificarContrase�a(usu,passw)){
					switch(priv){
					case 1:{
								new MenuRecepcionista(cod);
								dispose();
					}
					break;
					case 2: {
								new VentanaMedico(cod);
								dispose();
						}
					break;
					case 3:{
								new VentanaAdmin();
								dispose();
					}
					break;
					default: JOptionPane.showMessageDialog(null,"Error");
					}
				}
				else{
					JOptionPane.showMessageDialog(null,"Usuario o contrase�a erroneos!");
					NuevoIngreso();
				}
				rs1.close();
		}
		else {JOptionPane.showMessageDialog(null,"Usuario no existe!");
		NuevoIngreso();
		}
		cn.close();
		st.close();
		rs.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private boolean verificarContrase�a(String usu, String passw) {
		// TODO Auto-generated method stub
		try{
			Connection cn1=Conexion.getConexionMYSQL();
			Statement st1= cn1.createStatement();
			ResultSet rs1= st1.executeQuery("SELECT password FROM usuarios WHERE usuario ='"+usu+"'");
			while(rs1.next()){
				codigo1=rs1.getString(1);
			}
			cn1.close();
			st1.close();
			rs1.close();
				if(codigo1.equals(passw)){
					return true;
				}
				else {
					return false;
				}
			}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(txtUsuario)){
			txtPassword.requestFocus();
		}
		else if(e.getSource().equals(btnAceptar) || e.getSource().equals(txtPassword)){
			String contra=new String(txtPassword.getPassword());
			if(!txtUsuario.getText().equals("") && !contra.equals("")){
			String usu = txtUsuario.getText();
			char[] passw = txtPassword.getPassword();
			BuscarUsuario(usu,new String(passw));
			}
			else{
				JOptionPane.showMessageDialog(null,"campos vac�os");
			}
		}
	}
}
