package listas;

import javax.swing.JOptionPane;

import nodos.NodoHistoriales;
public class PilaHistoriales {
	private NodoHistoriales raiz;
	private int contador;
	public PilaHistoriales(){
		raiz=null;
		contador=0;
	}
	public void pop(){ //eliminar
		NodoHistoriales p=raiz;
		NodoHistoriales aux;
		if(raiz==null){
			JOptionPane.showMessageDialog(null,"No hay historiales");
		}
		else if(raiz.getNext()==null){
			raiz=null;
		}
		else{
			do{
				aux=p;
				p=p.getNext();
			}while(p.getNext()!=null);
			aux.setNext(null);
		}
		
	}
	public void push(NodoHistoriales nodo){ //agrega
		if(raiz==null){
			setRaiz(nodo);
			contador++;
		}
		else{
			NodoHistoriales aux=raiz;
			while(aux.getNext()!=null){
				aux=aux.getNext();
			}
			aux.setNext(nodo);
			contador++;
		}
	}
	public NodoHistoriales getRaiz() {
		return raiz;
	}
	public void setRaiz(NodoHistoriales raiz) {
		this.raiz = raiz;
	}
	public int getContador() {
		return contador;
	}
	public void setContador(int contador) {
		this.contador = contador;
	}
}
