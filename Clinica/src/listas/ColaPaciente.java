package listas;
import javax.swing.JOptionPane;

import nodos.NodoPaciente;

public class ColaPaciente {
	private NodoPaciente raiz;
	private int contador;
	public ColaPaciente(){
		setRaiz(null);
		setContador(0);
	}
	public void pop(){
		if(raiz==null){
			JOptionPane.showMessageDialog(null,"No hay nadie en la cola");
		}
		else{
			raiz=raiz.getNext();
		}
	}
	public void push(NodoPaciente p){
		if(raiz==null){
			setRaiz(p);
			contador++;
		}
		else{
			NodoPaciente aux=raiz;
			while(aux.getNext()!=null){
				aux=aux.getNext();
			}
			aux.setNext(p);
			contador++;
		}
	}
	public NodoPaciente getRaiz() {
		return raiz;
	}
	public void setRaiz(NodoPaciente raiz) {
		this.raiz = raiz;
	}
	public int getContador() {
		return contador;
	}
	public void setContador(int contador) {
		this.contador = contador;
	}
	
}
