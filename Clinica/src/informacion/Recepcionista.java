package informacion;

public class Recepcionista extends Persona {
	private String codigoRecepcionista;
	private String areaAtencion;
	public Recepcionista(String nombre, String apellido, int edad, String sexo,String telefono,
			String nacionalidad, String direccion, String estadoCivil,String dni,String codigoRecepcionista,String areaAtencion){
			super(nombre,apellido,edad,sexo,telefono,nacionalidad,direccion,estadoCivil,dni);
			this.setCodigoRecepcionista(codigoRecepcionista);
			this.setAreaAtencion(areaAtencion);
	}
	public String getCodigoRecepcionista() {
		return codigoRecepcionista;
	}
	public void setCodigoRecepcionista(String codigoRecepcionista) {
		this.codigoRecepcionista = codigoRecepcionista;
	}
	public String getAreaAtencion() {
		return areaAtencion;
	}
	public void setAreaAtencion(String areaAtencion) {
		this.areaAtencion = areaAtencion;
	}

}
