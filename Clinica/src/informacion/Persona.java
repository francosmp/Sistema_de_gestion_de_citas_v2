package informacion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import complementos.Conexion;
public class Persona {
	private String nombre;
	private String apellido;
	private int edad;
	private String sexo;
	private Date fechaNac;
	private String telefono;
	private String nacionalidad;
	private String direccion;
	private String estadoCivil;
	private String ocupacion;
	private String dni;
	private String codigop;
	public Persona(){};
	public Persona(String nombre, String apellido, int edad, String sexo, Date fechaNac, String telefono,
				String nacionalidad, String direccion, String estadoCivil, String ocupacion,String dni,String codigop) {
			super();
			this.nombre = nombre;
			this.apellido = apellido;
			this.edad = edad;
			this.sexo = sexo;
			this.fechaNac = fechaNac;
			this.telefono = telefono;
			this.nacionalidad = nacionalidad;
			this.direccion = direccion;
			this.estadoCivil = estadoCivil;
			this.ocupacion = ocupacion;
			this.dni=dni;
			this.codigop=codigop;
	}
	public Persona(String nombre, String apellido, int edad, String sexo,String telefono,
				String nacionalidad, String direccion, String estadoCivil,String dni){
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.sexo = sexo;
		this.telefono = telefono;
		this.nacionalidad = nacionalidad;
		this.direccion = direccion;
		this.estadoCivil = estadoCivil;
		this.dni=dni;
	}
	public void crearCodigoPac(){
		Connection cn=Conexion.getConexionMYSQL();
		int contador=0;
		try {
			Statement st=cn.createStatement();
			ResultSet rs=st.executeQuery("SELECT codigo FROM persona where persona.codigo like '%PAC%'");
			while(rs.next()){
				contador++;
			}
		String codigo="PAC"+String.valueOf(contador+1);
		this.codigop=codigo;
		cn.close();
		st.close();
		rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void crearCodigoRec(){
		Connection cn=Conexion.getConexionMYSQL();
		int contador=0;
		try {
			Statement st=cn.createStatement();
			ResultSet rs=st.executeQuery("SELECT codigo FROM persona where persona.codigo like '%REC%'");
			while(rs.next()){
				contador++;
			}
		String codigo="REC"+String.valueOf(contador+1);
		this.codigop=codigo;
		cn.close();
		st.close();
		rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Date getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getCodigop() {
		return codigop;
	}
	public void setCodigop(String codigop) {
		this.codigop = codigop;
	}
	
}
