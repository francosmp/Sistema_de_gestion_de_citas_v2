package informacion;

public class Historia {
	private String diagnostico;
	private String receta;
	private Cita cita;
	private String  descripcion;
	public Historia(String diagnostico, String receta, Cita cita,String descripcion) {
		super();
		this.diagnostico = diagnostico;
		this.receta = receta;
		this.cita = cita;
		this.descripcion=descripcion;
	}
	public String getDiagnostico() {
		return diagnostico;
	}
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	public String getReceta() {
		return receta;
	}
	public void setReceta(String receta) {
		this.receta = receta;
	}
	public Cita getCita() {
		return cita;
	}
	public void setCita(Cita cita) {
		this.cita = cita;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
