package paneles;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import informacion.Medico;
public class RMedico extends JPanel{
	private Medico medico;
	public RMedico() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{64, 131, 64, 122, 0};
		gridBagLayout.rowHeights = new int[]{20, 20, 20, 20, 20, 20, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblNombre = new JLabel("Nombre:");
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.anchor = GridBagConstraints.WEST;
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.gridx = 0;
		gbc_lblNombre.gridy = 0;
		add(lblNombre, gbc_lblNombre);
		
		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.anchor = GridBagConstraints.NORTH;
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 0;
		add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		rdbtnMasculino = new JRadioButton("Masculino");
		GridBagConstraints gbc_rdbtnMasculino = new GridBagConstraints();
		gbc_rdbtnMasculino.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnMasculino.gridx = 2;
		gbc_rdbtnMasculino.gridy = 0;
		add(rdbtnMasculino, gbc_rdbtnMasculino);
		
		rdbtnFemenino = new JRadioButton("Femenino");
		GridBagConstraints gbc_rdbtnFemenino = new GridBagConstraints();
		gbc_rdbtnFemenino.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnFemenino.gridx = 3;
		gbc_rdbtnFemenino.gridy = 0;
		add(rdbtnFemenino, gbc_rdbtnFemenino);
		
		JLabel lblApellido = new JLabel("Apellido:");
		GridBagConstraints gbc_lblApellido = new GridBagConstraints();
		gbc_lblApellido.anchor = GridBagConstraints.WEST;
		gbc_lblApellido.insets = new Insets(0, 0, 5, 5);
		gbc_lblApellido.gridx = 0;
		gbc_lblApellido.gridy = 1;
		add(lblApellido, gbc_lblApellido);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.anchor = GridBagConstraints.NORTH;
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		add(textField, gbc_textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("Dni:");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 2;
		gbc_lblNewLabel_6.gridy = 1;
		add(lblNewLabel_6, gbc_lblNewLabel_6);
		
		textField_4 = new JTextField();
		GridBagConstraints gbc_textField_4 = new GridBagConstraints();
		gbc_textField_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_4.anchor = GridBagConstraints.NORTH;
		gbc_textField_4.insets = new Insets(0, 0, 5, 0);
		gbc_textField_4.gridx = 3;
		gbc_textField_4.gridy = 1;
		add(textField_4, gbc_textField_4);
		textField_4.setColumns(10);
		
		JLabel lblEdad = new JLabel("Edad:");
		GridBagConstraints gbc_lblEdad = new GridBagConstraints();
		gbc_lblEdad.anchor = GridBagConstraints.WEST;
		gbc_lblEdad.insets = new Insets(0, 0, 5, 5);
		gbc_lblEdad.gridx = 0;
		gbc_lblEdad.gridy = 2;
		add(lblEdad, gbc_lblEdad);
		
		textField_2 = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.anchor = GridBagConstraints.NORTH;
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 2;
		add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Telefono:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 2;
		gbc_lblNewLabel_1.gridy = 2;
		add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		textField_5 = new JTextField();
		GridBagConstraints gbc_textField_5 = new GridBagConstraints();
		gbc_textField_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_5.anchor = GridBagConstraints.NORTH;
		gbc_textField_5.insets = new Insets(0, 0, 5, 0);
		gbc_textField_5.gridx = 3;
		gbc_textField_5.gridy = 2;
		add(textField_5, gbc_textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Estado civil:");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 3;
		add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		textField_6 = new JTextField();
		GridBagConstraints gbc_textField_6 = new GridBagConstraints();
		gbc_textField_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_6.anchor = GridBagConstraints.NORTH;
		gbc_textField_6.insets = new Insets(0, 0, 5, 5);
		gbc_textField_6.gridx = 1;
		gbc_textField_6.gridy = 3;
		add(textField_6, gbc_textField_6);
		textField_6.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Nacionalidad:");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 2;
		gbc_lblNewLabel_2.gridy = 3;
		add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		textField_7 = new JTextField();
		GridBagConstraints gbc_textField_7 = new GridBagConstraints();
		gbc_textField_7.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_7.anchor = GridBagConstraints.NORTH;
		gbc_textField_7.insets = new Insets(0, 0, 5, 0);
		gbc_textField_7.gridx = 3;
		gbc_textField_7.gridy = 3;
		add(textField_7, gbc_textField_7);
		textField_7.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Direccion:");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 4;
		add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		textField_8 = new JTextField();
		GridBagConstraints gbc_textField_8 = new GridBagConstraints();
		gbc_textField_8.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_8.anchor = GridBagConstraints.NORTH;
		gbc_textField_8.insets = new Insets(0, 0, 5, 5);
		gbc_textField_8.gridx = 1;
		gbc_textField_8.gridy = 4;
		add(textField_8, gbc_textField_8);
		textField_8.setColumns(10);
		
		JLabel lblEspecialidad = new JLabel("Area de atencion:");
		GridBagConstraints gbc_lblEspecialidad = new GridBagConstraints();
		gbc_lblEspecialidad.anchor = GridBagConstraints.WEST;
		gbc_lblEspecialidad.insets = new Insets(0, 0, 5, 0);
		gbc_lblEspecialidad.gridwidth = 2;
		gbc_lblEspecialidad.gridx = 2;
		gbc_lblEspecialidad.gridy = 4;
		add(lblEspecialidad, gbc_lblEspecialidad);
		
		textField_10 = new JTextField();
		GridBagConstraints gbc_textField_10 = new GridBagConstraints();
		gbc_textField_10.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_10.anchor = GridBagConstraints.NORTH;
		gbc_textField_10.insets = new Insets(0, 0, 5, 0);
		gbc_textField_10.gridx = 3;
		gbc_textField_10.gridy = 4;
		add(textField_10, gbc_textField_10);
		textField_10.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("Consultorio:");
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridwidth = 2;
		gbc_lblNewLabel_7.gridx = 0;
		gbc_lblNewLabel_7.gridy = 5;
		add(lblNewLabel_7, gbc_lblNewLabel_7);
		
		textField_12 = new JTextField();
		GridBagConstraints gbc_textField_12 = new GridBagConstraints();
		gbc_textField_12.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_12.anchor = GridBagConstraints.NORTH;
		gbc_textField_12.insets = new Insets(0, 0, 5, 5);
		gbc_textField_12.gridx = 1;
		gbc_textField_12.gridy = 5;
		add(textField_12, gbc_textField_12);
		textField_12.setColumns(10);
		
		JLabel lblHoraInicio = new JLabel("Hora Inicio:");
		GridBagConstraints gbc_lblHoraInicio = new GridBagConstraints();
		gbc_lblHoraInicio.anchor = GridBagConstraints.WEST;
		gbc_lblHoraInicio.insets = new Insets(0, 0, 5, 5);
		gbc_lblHoraInicio.gridx = 2;
		gbc_lblHoraInicio.gridy = 5;
		add(lblHoraInicio, gbc_lblHoraInicio);
		
		textField_13 = new JTextField();
		GridBagConstraints gbc_textField_13 = new GridBagConstraints();
		gbc_textField_13.insets = new Insets(0, 0, 5, 0);
		gbc_textField_13.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_13.gridx = 3;
		gbc_textField_13.gridy = 5;
		add(textField_13, gbc_textField_13);
		textField_13.setColumns(10);
		
		JLabel lblTurno = new JLabel("Turno:");
		GridBagConstraints gbc_lblTurno = new GridBagConstraints();
		gbc_lblTurno.anchor = GridBagConstraints.WEST;
		gbc_lblTurno.insets = new Insets(0, 0, 5, 5);
		gbc_lblTurno.gridx = 0;
		gbc_lblTurno.gridy = 6;
		add(lblTurno, gbc_lblTurno);
		
		textField_9 = new JTextField();
		GridBagConstraints gbc_textField_9 = new GridBagConstraints();
		gbc_textField_9.insets = new Insets(0, 0, 5, 5);
		gbc_textField_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_9.gridx = 1;
		gbc_textField_9.gridy = 6;
		add(textField_9, gbc_textField_9);
		textField_9.setColumns(10);
		
		JLabel lblHoraFin = new JLabel("Hora Fin:");
		GridBagConstraints gbc_lblHoraFin = new GridBagConstraints();
		gbc_lblHoraFin.anchor = GridBagConstraints.WEST;
		gbc_lblHoraFin.insets = new Insets(0, 0, 5, 5);
		gbc_lblHoraFin.gridx = 2;
		gbc_lblHoraFin.gridy = 6;
		add(lblHoraFin, gbc_lblHoraFin);
		
		textField_14 = new JTextField();
		GridBagConstraints gbc_textField_14 = new GridBagConstraints();
		gbc_textField_14.insets = new Insets(0, 0, 5, 0);
		gbc_textField_14.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_14.gridx = 3;
		gbc_textField_14.gridy = 6;
		add(textField_14, gbc_textField_14);
		textField_14.setColumns(10);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_10;
	private JTextField textField_12;
	private JTextField textField_9;
	private JTextField textField_13;
	private JTextField textField_14;
	JRadioButton rdbtnMasculino;
	JRadioButton rdbtnFemenino;

	public boolean isEmpty(){
		if(textField.getText().equals("") && textField_1.getText().equals("") && textField_2.getText().equals("")
			&& textField_4.getText().equals("") && textField_5.getText().equals("") && textField_6.getText().equals("")
			&& textField_6.getText().equals("") && textField_7.getText().equals("") && textField_7.getText().equals("")
			&& textField_8.getText().equals("") && textField_9.getText().equals("") && textField_10.getText().equals("")
			&& textField_12.getText().equals("") && textField_13.getText().equals("") && textField_14.getText().equals("")
			&& !rdbtnMasculino.isSelected() && !rdbtnFemenino.isSelected() ){
			return true;
		}
		else return false;
	}
	public void llenar(){
		String sexo;
		if(rdbtnMasculino.isSelected()){
			sexo="M";
		}
		else{
			sexo="F";
		}
		this.medico=new Medico();
		medico.setNombre(textField_1.getText());
		medico.setApellido(textField.getText());
		medico.setEdad(Integer.valueOf(textField_2.getText()));
		medico.setSexo(sexo);
		medico.setTelefono(textField_5.getText());
		medico.setNacionalidad(textField_7.getText());
		medico.setDireccion(textField_8.getText());
		medico.setEstadoCivil(textField_6.getText());
		medico.setDni(textField_4.getText());
		medico.setNumConsultorio(Integer.valueOf(textField_12.getText()));
		medico.setEspecialidad(textField_10.getText());
		medico.setDias(textField_9.getText());
		medico.setHoraInicio(textField_13.getText());
		medico.setHoraFin(textField_14.getText());
		medico.crearCodigoMed();
	}
	public void nuevoIngreso(){
		textField_1.setText("");
		textField.setText("");
		textField_2.setText("");
		textField_5.setText("");
		textField_7.setText("");
		textField_8.setText("");
		textField_6.setText("");
		textField_4.setText("");
		textField_12.setText("");
		textField_10.setText("");
		textField_9.setText("");
		textField_13.setText("");
		textField_14.setText("");
		medico=null;
	}
	public Medico getMedico() {
		return medico;
	}
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
}