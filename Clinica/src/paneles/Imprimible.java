package paneles;
import complementos.Fondo;
import informacion.Cita;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JLabel;
public class Imprimible extends Fondo{
	public Imprimible(Cita cita) {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{95, 191, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{37, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblClnicaWalde = new JLabel("CL\u00CDNICA WALDE");
		GridBagConstraints gbc_lblClnicaWalde = new GridBagConstraints();
		gbc_lblClnicaWalde.insets = new Insets(0, 0, 5, 5);
		gbc_lblClnicaWalde.gridx = 1;
		gbc_lblClnicaWalde.gridy = 0;
		add(lblClnicaWalde, gbc_lblClnicaWalde);
		
		JLabel lblCodigoDelPaciente = new JLabel("Codigo del Paciente:");
		GridBagConstraints gbc_lblCodigoDelPaciente = new GridBagConstraints();
		gbc_lblCodigoDelPaciente.anchor = GridBagConstraints.WEST;
		gbc_lblCodigoDelPaciente.insets = new Insets(0, 0, 5, 5);
		gbc_lblCodigoDelPaciente.gridx = 2;
		gbc_lblCodigoDelPaciente.gridy = 1;
		add(lblCodigoDelPaciente, gbc_lblCodigoDelPaciente);
		
		JLabel lblNewLabel = new JLabel(cita.getPaciente().getCodigop());
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 3;
		gbc_lblNewLabel.gridy = 1;
		add(lblNewLabel, gbc_lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Fecha:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 2;
		gbc_lblNewLabel_1.gridy = 2;
		add(lblNewLabel_1, gbc_lblNewLabel_1);
		Date fecha=new Date();
		JLabel lblNewLabel_2 = new JLabel(fecha.toString());
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2.gridx = 3;
		gbc_lblNewLabel_2.gridy = 2;
		add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JLabel lblFechaDeLa = new JLabel("Fecha de la Cita:");
		GridBagConstraints gbc_lblFechaDeLa = new GridBagConstraints();
		gbc_lblFechaDeLa.anchor = GridBagConstraints.WEST;
		gbc_lblFechaDeLa.insets = new Insets(0, 0, 5, 5);
		gbc_lblFechaDeLa.gridx = 2;
		gbc_lblFechaDeLa.gridy = 3;
		add(lblFechaDeLa, gbc_lblFechaDeLa);
		
		JLabel lblNewLabel_3 = new JLabel(cita.getFechaCita().toString());
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_3.gridx = 3;
		gbc_lblNewLabel_3.gridy = 3;
		add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		JLabel lblNombreCompleto = new JLabel("Nombre completo:");
		GridBagConstraints gbc_lblNombreCompleto = new GridBagConstraints();
		gbc_lblNombreCompleto.anchor = GridBagConstraints.WEST;
		gbc_lblNombreCompleto.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreCompleto.gridx = 0;
		gbc_lblNombreCompleto.gridy = 4;
		add(lblNombreCompleto, gbc_lblNombreCompleto);
		
		JLabel lblNewLabel_4 = new JLabel(cita.getPaciente().getNombre()+" "+cita.getPaciente().getApellido());
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 1;
		gbc_lblNewLabel_4.gridy = 4;
		add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		JLabel lblDni = new JLabel("DNI:");
		GridBagConstraints gbc_lblDni = new GridBagConstraints();
		gbc_lblDni.anchor = GridBagConstraints.WEST;
		gbc_lblDni.insets = new Insets(0, 0, 5, 5);
		gbc_lblDni.gridx = 0;
		gbc_lblDni.gridy = 5;
		add(lblDni, gbc_lblDni);
		
		JLabel lblNewLabel_5 = new JLabel(cita.getPaciente().getDni());
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 1;
		gbc_lblNewLabel_5.gridy = 5;
		add(lblNewLabel_5, gbc_lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Lugar de atenci\u00F3n:");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 0;
		gbc_lblNewLabel_6.gridy = 6;
		add(lblNewLabel_6, gbc_lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel(cita.getMedico().getDireccion());
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 1;
		gbc_lblNewLabel_7.gridy = 6;
		add(lblNewLabel_7, gbc_lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("M\u00E9dico:");
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 0;
		gbc_lblNewLabel_8.gridy = 7;
		add(lblNewLabel_8, gbc_lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel(cita.getMedico().getNombre()+" "+cita.getMedico().getApellido());
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.gridx = 1;
		gbc_lblNewLabel_9.gridy = 7;
		add(lblNewLabel_9, gbc_lblNewLabel_9);
		
		JLabel lblEspecialidad = new JLabel("Especialidad:");
		GridBagConstraints gbc_lblEspecialidad = new GridBagConstraints();
		gbc_lblEspecialidad.anchor = GridBagConstraints.WEST;
		gbc_lblEspecialidad.insets = new Insets(0, 0, 5, 5);
		gbc_lblEspecialidad.gridx = 0;
		gbc_lblEspecialidad.gridy = 8;
		add(lblEspecialidad, gbc_lblEspecialidad);
		
		JLabel lblNewLabel_10 = new JLabel(cita.getMedico().getEspecialidad());
		GridBagConstraints gbc_lblNewLabel_10 = new GridBagConstraints();
		gbc_lblNewLabel_10.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_10.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_10.gridx = 1;
		gbc_lblNewLabel_10.gridy = 8;
		add(lblNewLabel_10, gbc_lblNewLabel_10);
		
		JLabel lblgraciasPorSu = new JLabel("\u00A1Gracias por su preferencia!");
		GridBagConstraints gbc_lblgraciasPorSu = new GridBagConstraints();
		gbc_lblgraciasPorSu.insets = new Insets(0, 0, 0, 5);
		gbc_lblgraciasPorSu.gridx = 1;
		gbc_lblgraciasPorSu.gridy = 10;
		add(lblgraciasPorSu, gbc_lblgraciasPorSu);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
