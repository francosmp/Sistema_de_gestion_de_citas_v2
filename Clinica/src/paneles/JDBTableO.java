package paneles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableRowSorter;

import complementos.MyTableModel;
import frames.Diagnostico;
import informacion.Historia;
import listas.Historiales;
import nodos.NodoHistorial;
public class JDBTableO extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Historiales historias;
	JTable table;
	ArrayList<JButton> botones;
	private MyTableModel modelo;
	JScrollPane scroll;
	public JDBTableO(Historiales historias){
		super();
		table= new JTable(){
	        /**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			public boolean isCellEditable(int rowIndex, int vColIndex) {
	        	if(vColIndex!=2)
	        		return false;
	        	else
	        		return true;
	        }};
	       scroll=new JScrollPane();
		this.historias=historias;
		dataBaseInfo();
	}
	public void dataBaseInfo() {
		ArrayList<Object[]> data = new ArrayList<Object[]>();
		botones = new ArrayList<JButton>();
		int i=0;
		int j=0;
		NodoHistorial aux=historias.getRaiz();
			do{
				if(!historias.getHistoria(j).getDescripcion().equals("-----")){
				botones.add(new JButton("Ver detalles"));
				botones.get(i).addActionListener(this);
				data.add(new Object[]{aux.getHistoria().getDescripcion(),
						aux.getHistoria().getCita().getFechaCita(),
						botones.get(i)});
				i++;
				}
				j++;
				aux=aux.getNext();
			}while(aux!=null);
			Object[][] o= data.toArray(new Object[data.size()][3]);
			setTable(o);
			scroll.setViewportView(table);
			this.add(scroll);
	}
	private void setTable(Object[][] data){
		modelo = new MyTableModel(new String[]{"Descipcion","Fecha","Ver detalles"},data);
		TableRowSorter<MyTableModel> sorter = new TableRowSorter<MyTableModel>(modelo);
		table.setModel(modelo);
		table.setDefaultRenderer(JButton.class, new complementos.ButtonCellRenderer());
		table.setDefaultEditor(JButton.class, new complementos.ButtonCellEditor());
		table.setRowSorter(sorter);
		table.setRowSelectionAllowed(false);
		table.getTableHeader().setReorderingAllowed(false);
}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		int cantBotones=botones.size();
		for(int i=0; i<cantBotones; i++){
			if(e.getSource()==botones.get(i)){
				Historia h=historias.getHistoria(i);
				new Diagnostico(h);
			}
		}
	}
}