package paneles;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import complementos.Conexion;

public class Modificar extends JPanel implements ActionListener{
	public Modificar(String codigoMedico) {
		setLayout(new BorderLayout(0, 0));
		flag=false;
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		
		JLabel lblCodigoDeCita = new JLabel("Codigo de cita:");
		panel.add(lblCodigoDeCita);
		
		textField_1 = new JTextField();
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblCdigoDeAcceso = new JLabel("C\u00F3digo de acceso:");
		panel.add(lblCdigoDeAcceso);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);
		
		btnNewButton = new JButton("Acceder");
		btnNewButton.addActionListener(this);
		panel.add(btnNewButton);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static JTextField textField;
	public static JTextField textField_1;
	private JButton btnNewButton;
	private UpdateDatos ud;
	private boolean flag;
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(btnNewButton)){
			Connection cn=Conexion.getConexionMYSQL();
			try{
				Statement st=cn.createStatement();
				ResultSet rs1=st.executeQuery("select permiso from citas where codigoCita='"+textField_1.getText()+"';");
				if(rs1.next()){
					if(rs1.getBoolean("permiso")){
						ResultSet rs=st.executeQuery("select acceso from citas where codigoCita='"+textField_1.getText()+"';");
						if(rs.next()){
							if(rs.getString("acceso").equals(textField.getText())){
								if(flag){
									this.remove(ud);
								}
								ud=new UpdateDatos(textField.getText(),textField_1.getText());
								this.add(ud,BorderLayout.CENTER);
								this.updateUI();
							}
					}
				}
				else{
					JOptionPane.showMessageDialog(null,"No tiene permiso para modificar");
					}
				}
				else{
					JOptionPane.showMessageDialog(null,"Codigo de cita erroneo o vacio");
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

}
