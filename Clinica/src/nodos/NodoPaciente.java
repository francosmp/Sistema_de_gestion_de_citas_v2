package nodos;

import informacion.Persona;

public class NodoPaciente {
	private Persona paciente;
	private NodoPaciente next;
	public NodoPaciente(Persona p){
		paciente=p;
		next=null;
	}
	public Persona getPaciente() {
		return paciente;
	}
	public void setPaciente(Persona paciente) {
		this.paciente = paciente;
	}
	public NodoPaciente getNext() {
		return next;
	}
	public void setNext(NodoPaciente next) {
		this.next = next;
	}
}
